<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Database Seeding</title>

       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    
    </head>
    <body class=" container  mt-5">
    	<h1 class="text-center text-info ">
      Users
    </h1>
  <ul class="list-group">
  @foreach($users as $user)
  <li class="list-group-item list-group-item-info">
     <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1">{{$user->name}}</h5>
          <p class="mb-1">{{$user->email}}</p>
      <small> {{$user->created_at->diffForHumans()}}</small>
    </div>
  </li>
   @endforeach
</ul>
</body>
</html>